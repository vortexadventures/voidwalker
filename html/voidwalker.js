// API coupling;
var voidBase = "https://api.the-vortex.nl/";
var voidUser;
var voidPass;

var walkerPath = "/"
var walkerBase = "https://voidwalker.the-vortex.nl" + walkerPath;

/***
	We will store actions/calls for the API in here before trying to
	execute them. This gives us a way to gracefully recover if the
	network is down.
*/
var voidActions;

function storeAction(type, data) {
	data.type = type;
	voidActions.push(data);
	editor.voidData.changeCount = voidActions.length;
}

function resetActions() {
	voidActions = [];
}

function executeActions() {
	/*
		Iterates over the stored actions and try to execute it. When the
		action is done, it will call this function again to go on to the
		next action.
	 */
	if (voidActions.length) {
		switch(voidActions[0].type) {
			case "add":
				voidPut(voidActions[0].url, voidActions[0].data, function(result) {
					if (voidActions[0].callback) {
						voidActions[0].callback(voidActions[0].data, result);
					}
					voidActions.shift();
					executeActions();
				});
			break;
			case "delete":
				voidDelete(voidActions[0].url, function(result) {
					// maybe check if the result was true?
					voidActions.shift();
					executeActions();
				});
			break;
			case "redirect":
				if (voidActions.length > 1) {
					console.log("Redirect isn't the last action in the queue?")
				} else {
					document.location = walkerBase + voidActions[0].url;
					voidActions.shift();
				}
			break;
			default:
				console.log("Unknown handles action "+voidActions[0].type);
				voidActions.shift();
			break;
		}
	}
	editor.voidData.changeCount = voidActions.length;
}

function validateToken() {
	if (!localStorage.expiry) {
		return false;
	}

	if (localStorage.expiry > Math.floor(Date.now() / 1000) + 60) {
		return true;
	}
	console.log('login token expired');
	return false;
}

function pageMode() {
	var currentPath = document.body.getAttribute("data-simply-path");
	if (currentPath.search("^"+walkerPath+"$") > -1) {
		return "root";
	} else if (currentPath.search("^"+walkerPath+"players/") > -1) {
		return "player";
	} else if (currentPath.search("^"+walkerPath+"characters/") > -1) {
		return "character";
	} else if (currentPath.search("^"+walkerPath+"conditions/") > -1) {
		return "condition";
	} else if (currentPath.search("^"+walkerPath+"powers/") > -1) {
		return "power";
	} else if (currentPath.search("^"+walkerPath+"items/") > -1) {
		return "item";
	}
	return "";
}

document.addEventListener("click", function(evt) {
	/*
		A generic handler for buttons, hyperlinks and other elements that
		can be clicked. This allows us to just give the button an HTML
		attribute and set up handling for the button in here.
	*/
	// Find the button that was clicked;
	var clickTarget = evt.target;
	if (clickTarget.tagName.toLowerCase() == "span") {
		clickTarget = clickTarget.parentNode;
	}
	if (clickTarget.tagName.toLowerCase() == "img") {
		clickTarget = clickTarget.parentNode;
	}
	var action = clickTarget.getAttribute("data-action");
	if (action === null) {
		if (clickTarget.href == document.location.href) {
			editor.fireEvent("hashchange", window);
		}
		return;
	}

	var removeListItem = function(target) {
		/* Handler for remove buttons - just remove the list item, data binding will take care of the rest */
		while (target && !target.getAttribute("data-simply-list-item")) {
			target = target.parentNode;
		}
		if (target) {
			target.parentNode.removeChild(target);
		}
	};

	evt.preventDefault();
	switch (action) {
		case "voidwalker-remove":
			removeListItem(clickTarget);
		break;
		case "voidwalker-login":
			voidLogout();

			voidUser = document.getElementById("username").value;
			voidPass = document.getElementById("password").value;
			voidLogin(function(result) {
				if(result.plin) {
					document.location = walkerBase + "players/#" + result.plin;
				}
			});
		break;
		case "voidwalker-logout":
			voidLogout();
			document.location = walkerBase;
		break;
		case "voidwalker-login-social-discord":
			//FIXME should use reponse of voidGET('/auth/social')
			// it lists the supported login providers, and it contains the
			// authUri and login links for each of the providers.
			// This list should also be used to create the buttons.
			authUri = "https:\/\/discordapp.com\/api\/oauth2\/authorize?client_id=1137466357764595903\u0026redirect_uri=CALLBACK\u0026response_type=code\u0026state=STATE\u0026scope=identify+email";
			login = "\/auth\/social\/discord?code=CODE\u0026redirect_uri=CALLBACK";
			socialAuthentication(authUri, login);
		break;
		case "voidwalker-login-social-google":
			authUri = "https:\/\/accounts.google.com\/o\/oauth2\/auth?client_id=502868829974-hrsk0vhb1scfj5rkfbfg30dpv3jncju4.apps.googleusercontent.com\u0026redirect_uri=CALLBACK\u0026response_type=code\u0026scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile\u0026state=STATE";
			login = "\/auth\/social\/google?code=CODE\u0026redirect_uri=CALLBACK";
			socialAuthentication(authUri, login);
		break;
		case "voidwalker-login-social-gitlab":
			authUri = "https:\/\/gitlab.com\/oauth\/authorize?client_id=e92c1060b3e7701dc21a5c4e4d24e0800e884b374f4d98dc7f9b41df42b817c3\u0026redirect_uri=CALLBACK\u0026response_type=code\u0026state=STATE\u0026scope=read_user";
			login = "\/auth\/social\/gitlab?code=CODE\u0026redirect_uri=CALLBACK";
			socialAuthentication(authUri, login);
		break;
		case "voidwalker-print":
			if (!editor.voidData.url) {
				console.log("Hmm, geen pageData.url.  Nieuw / nog niet opgeslagen object?");
				return;
			}
			voidPost(editor.voidData.url + "/print");
		break;
		case "voidwalker-print-all":
			if (!editor.voidData.url) {
				console.log("Hmm, geen pageData.url.  Nieuw / nog niet opgeslagen object?");
				return;
			}
			voidPost(editor.voidData.url + "/print", "all");
		break;
		case "voidwalker-add-attribute":
			var code = document.querySelector(".attribute-add select").value;
			var attribute = editor.dataSources.attributes.load.search("search > * > *", "code[value='" + code.toLowerCase() + "']")[0];
			if (attribute) {
				var i = editor.voidData.attributes.list.findIndex(function(e) {
					return e.attribute.id == attribute.id;
				});
				if (i < 0) {
					editor.voidData.attributes.list.push({attribute : attribute});
					editor.voidData.attributes.list.sort(function(a, b) {
						return a.attribute.id - b.attribute.id;
					});
					if (editor.voidData.url) {
						storeAction("add", {url : editor.voidData.url + "/attributes", data : {attribute_id : attribute.id}});
					}
				}
			}
		break;
		case "voidwalker-remove-attribute":
			if (editor.voidData.url) {
				var attributeId = clickTarget.querySelector("[data-simply-field='attribute.id']").value;
				storeAction("delete", {url : editor.voidData.url + "/attributes/" + attributeId});
			}
			removeListItem(clickTarget);
		break;
		case "voidwalker-add-character":
			var plinchin = document.querySelector(".character-add select").value;
			var plin = plinchin.split('/')[0];
			var chin = plinchin.split('/')[1];
			var expires = document.querySelector("#character-expires").value;
			var data = {expiry : expires};
			if (!expires) {
				expires = "Until death";
			}

			var character = editor.dataSources.characters.load.search("search > * > *", "plin[value='" + plin + "'] ~ chin[value='" + chin + "']")[0];
			if (character) {
				var i = editor.voidData.characters.list.findIndex(function(e) {
					return e.character.plin == plin && e.character.chin == chin;
				});

				if (i < 0) {
					character.link = walkerBase + "characters/#" + plin + "/" + chin;
					editor.voidData.characters.list.push({character : character, expiry: expires});
					editor.voidData.characters.list.sort(function(a, b) {
						if(a.character.plin == b.character.plin)
							return a.character.chin - b.character.chin;
						return a.character.plin - b.character.plin;
					});
				} else {
					editor.voidData.characters.list[i].expiry = expires;
				}

				if (editor.voidData.url) {
					var addUrl = character.url;
					if (editor.voidData.coin && editor.voidData.coin != "New") {
						addUrl += "/conditions";
						data.condition_id = editor.voidData.coin;
					} else if (editor.voidData.poin && editor.voidData.poin != "New") {
						addUrl += "/powers";
						data.power_id = editor.voidData.poin;
					}
					storeAction("add", {url : addUrl, data : data});
				}
			}
		break;
		case "voidwalker-remove-character":
			var plin = clickTarget.querySelector("[data-simply-field='character.plin']").value;
			var chin = clickTarget.querySelector("[data-simply-field='character.chin']").value;
			var character = editor.dataSources.characters.load.search("search > * > *", "plin[value='" + plin + "'] ~ chin[value='" + chin + "']")[0];
			if (character && editor.voidData.url) {
				if (editor.voidData.coin) {
					storeAction("delete", {url : character.url + "/conditions/" + editor.voidData.coin});
				} else if (editor.voidData.poin) {
					storeAction("delete", {url : character.url + "/powers/" + editor.voidData.poin});
				}
			}
			removeListItem(clickTarget);
		break;
		case "voidwalker-add-item":
			var itin = document.querySelector(".item-add select").value;
			var item = editor.dataSources.items.load.search("search > * > *", "itin[value='" + itin + "']")[0];
			if (item) {
				var i = editor.voidData.items.list.findIndex(function(e) {
					return e.itin == itin;
				});
				if (i < 0) {
					item.link = {href: walkerBase + "items/#" + item.itin};
					editor.voidData.items.list.push(item);
					editor.voidData.items.list.sort(function(a, b) {
						return a.itin - b.itin;
					});
					if (editor.voidData.url) {
						storeAction("add", {url : item.url, data : {plin: editor.voidData.plin, chin: editor.voidData.chin}});
					}
				}
			}
		break;
		case "voidwalker-print-item":
			var itin = clickTarget.querySelector("[data-simply-field='itin']").value;
			var item = editor.dataSources.items.load.search("search > * > *", "itin[value='" + itin + "']")[0];
			if (item && editor.voidData.url) {
				voidPost(item.url + "/print");
			}
		break;
		case "voidwalker-remove-item":
			var itin = clickTarget.querySelector("[data-simply-field='itin']").value;
			var item = editor.dataSources.items.load.search("search > * > *", "itin[value='" + itin + "']")[0];
			if (item && editor.voidData.url) {
				storeAction("add", {url : item.url, data : {plin: null, chin: null}});
			}
			removeListItem(clickTarget);
		break;
		case "voidwalker-add-skill":
			var skillName = document.querySelector(".skill-add select").value;
			var skillIx = editor.dataSources.skills.load.findIndex(function(e) {
				return e.name == skillName;
			});
			if (skillIx >= 0) {
				var j = -1;
				for (var i=0; i<editor.voidData.skills.list.length; i++) {
					skillId = editor.voidData.skills.list[i].skill.id;
					j = editor.dataSources.skills.load.findIndex(function(e) {
							return e.id == skillId;
						});
					if (j >= skillIx) {
						break;
					}

				}
				if (j != skillIx) {
					var selectedSkill = editor.dataSources.skills.load[skillIx];
					data = { skill : selectedSkill, times : 1, name : selectedSkill.name + " (1x)", cost : selectedSkill.cost };
					editor.voidData.skills.list.splice(i, 0, data);
					if (editor.voidData.url) {
						storeAction("add", {url : editor.voidData.url + "/skills", data : {skill_id : selectedSkill.id, times : 1}});
					}
					updateSkillPoints();
				}
			}
		break;
		case "voidwalker-remove-skill":
			var skillId = clickTarget.querySelector("[data-simply-field='skill.id']").value;
			for (var i=0; i<editor.voidData.skills.list.length; i++) {
				var item = editor.voidData.skills.list[i];
				if(item.skill.id != skillId) {
					continue;
				}
				var times = --item.times;

				if(times == 0) {
					storeAction("delete", {url : editor.voidData.url + "/skills/" + skillId});
					removeListItem(clickTarget);
					updateSkillPoints();
					break;
				}

				item.name = item.skill.name + ' (' + times + 'x)';
				item.cost = item.skill.cost * times;
				if(item.skill.mana_amount > 0) {
					item.mana_amount = item.skill.mana_amount * times;
				}

				if (editor.voidData.url) {
					storeAction("add", {url : editor.voidData.url + "/skills/" + skillId, data : {times : times}});
				}
				updateSkillPoints();
				break;
			}
		break;
		case "voidwalker-plus-skill":
			var skillId = clickTarget.querySelector("[data-simply-field='skill.id']").value;
			for (var i=0; i<editor.voidData.skills.list.length; i++) {
				if(editor.voidData.skills.list[i].skill.id != skillId) {
					continue;
				}
				if(editor.voidData.skills.list[i].times >= editor.voidData.skills.list[i].skill.times_max) {
					break;
				}
				var times = ++editor.voidData.skills.list[i].times;

				editor.voidData.skills.list[i].name = editor.voidData.skills.list[i].skill.name + ' (' + times + 'x)';
				editor.voidData.skills.list[i].cost = editor.voidData.skills.list[i].skill.cost * times;
				if(editor.voidData.skills.list[i].skill.mana_amount > 0) {
					editor.voidData.skills.list[i].mana_amount = editor.voidData.skills.list[i].skill.mana_amount * times;
				}

				if (editor.voidData.url) {
					storeAction("add", {url : editor.voidData.url + "/skills/" + skillId, data : {times : times}});
				}
				updateSkillPoints();
				break;
			}
		break;
		case "voidwalker-add-power-condition":
			var pcin = document.querySelector(".power-condition-add select").value;
			var expires = document.querySelector("#character-power-condition-expires").value;
			var data = {expiry : expires};
			if (!expires) {
				expires = "Until death";
			}
			var entry = {expiry: expires};

			var pc = pcin[0];
			pcin = pcin.slice(1);

			var powerCondition = editor.dataSources.powers.load.search("search > * > *", "pcin[value='" + pcin + "']")[0];
			if (powerCondition && pc == "p") {
				var i = editor.voidData.powers.list.findIndex(function(e) {
					return e.power.poin == pcin;
				});
				if (i < 0) {
					entry.link = {href: walkerBase + "powers/#" + pcin};
					entry.power = powerCondition;
					editor.voidData.powers.list.push(entry);
					editor.voidData.powers.list.sort(function(a, b) {
						return a.power.poin - b.power.poin;
					});
				} else {
					editor.voidData.powers.list[i].expiry = expires;
				}
				if (editor.voidData.url) {
					data.power_id = pcin;
					storeAction("add", {url : editor.voidData.url + "/powers", data : data});
				}
			} else {
				powerCondition = editor.dataSources.conditions.load.search("search > * > *", "pcin[value='" + pcin + "']")[0];
				if (powerCondition) {
					var i = editor.voidData.conditions.list.findIndex(function(e) {
						return e.condition.coin == pcin;
					});
					if (i < 0) {
						entry.link = {href: walkerBase + "conditions/#" + pcin};
						entry.condition = powerCondition;
						editor.voidData.conditions.list.push(entry);
						editor.voidData.conditions.list.sort(function(a, b) {
							return a.condition.coin - b.condition.coin;
						});
					} else {
						editor.voidData.conditions.list[i].expiry = expires;
					}
					if (editor.voidData.url) {
						data.condition_id = pcin;
						storeAction("add", {url : editor.voidData.url + "/conditions", data : data});
					}
				}
			}
		break;
		case "voidwalker-print-power":
			if (editor.voidData.url) {
				var powerId = clickTarget.querySelector("[data-simply-field='power.poin']").value;
				voidPost(editor.voidData.url + "/powers/" + powerId + "/print");
			}
		break;
		case "voidwalker-print-power-character":
		case "voidwalker-print-condition-character":
			var plin = clickTarget.querySelector("[data-simply-field='character.plin']").value;
			var chin = clickTarget.querySelector("[data-simply-field='character.chin']").value;
			var character = editor.dataSources.characters.load.search("search > * > *", "plin[value='" + plin + "'] ~ chin[value='" + chin + "']")[0];
			if (editor.voidData.url && character) {
				if (editor.voidData.coin) {
					voidPost(character.url + "/conditions/" + editor.voidData.coin + "/print");
				} else if (editor.voidData.poin) {
					voidPost(character.url + "/powers/" + editor.voidData.poin + "/print");
				}
			}
		break;
		case "voidwalker-print-condition":
			if (editor.voidData.url) {
				var conditionId = clickTarget.querySelector("[data-simply-field='condition.coin']").value;
				voidPost(editor.voidData.url + "/conditions/" + conditionId + "/print");
			}
		break;
		case "voidwalker-remove-power":
			if (editor.voidData.url) {
				var powerId = clickTarget.querySelector("[data-simply-field='power.poin']").value;
				storeAction("delete", {url : editor.voidData.url + "/powers/" + powerId});
			}
			removeListItem(clickTarget);
		break;
		case "voidwalker-remove-condition":
			if (editor.voidData.url) {
				var conditionId = clickTarget.querySelector("[data-simply-field='condition.coin']").value;
				storeAction("delete", {url : editor.voidData.url + "/conditions/" + conditionId});
			}
			removeListItem(clickTarget);
		break;
		case "voidwalker-item-expiry-1":
			if (pageMode() == "item") {
				editor.voidData.expiry = addMonths(editor.voidData.expiry, 1);
			}
		break;
		case "voidwalker-item-expiry-12":
			if (pageMode() == "item") {
				editor.voidData.expiry = addMonths(editor.voidData.expiry, 12);
			}
		break;
		case "voidwalker-save":
			var addUrl = editor.voidData.url;
			var data = JSON.parse(JSON.stringify(editor.voidData));
			var currentPath = document.body.getAttribute("data-simply-path");
			switch (pageMode()) {
				case "player":
					if(!editor.voidData.url) {
						addUrl = "players";
					}
					if(typeof editor.voidData.password !== "string") {
						delete data.password;
					}
					storeAction("add", {url : addUrl, data : data, callback : function(data, result) {
						if (result && result.location) {
							editor.voidData.url = result.location;
						}
					}});
				break;
				case "character":
					if(!editor.voidData.url) {
						addUrl = "players/" + editor.voidData.plin + "/characters";
						delete data.chin;
					}
					storeAction("add", {url : addUrl, data : data, callback : function(data, result) {
						/* Add skills, powers, conditions and items to the character after it has been created. */
						if (result && result.location) {
							data.url = result.location;
							data.chin = result.location.split("/")[3];
							editor.voidData.url = data.url;
							editor.voidData.chin = data.chin;

							for (var i=0; i<data.skills.list.length; i++) {
								storeAction("add", {url : data.url + "/skills", data : {skill_id : data.skills.list[i].skill.id, times : data.skills.list[i].times}});
							}
							for (var i=0; i<data.powers.list.length; i++) {
								storeAction("add", {url : data.url + "/powers", data : {power_id : data.powers.list[i].power.poin}});
							}
							for (var i=0; i<data.conditions.list.length; i++) {
								storeAction("add", {url : data.url + "/conditions", data : {condition_id : data.conditions.list[i].condition.coin}});
							}
							for (var i=0; i<data.items.list.length; i++) {
								storeAction("add", {url : data.items.list[i].url, data : {plin : data.plin, chin : data.chin}});
							}
							storeAction("redirect", {url : "characters/#" + data.plin + "/" + data.chin});
						}
					}});
				break;
				case "condition":
					if(!editor.voidData.url) {
						addUrl = "conditions";
						delete data.coin;
					}
					storeAction("add", {url : addUrl, data : data, callback : function(data, result) {
						/* Add the condition to the specified owner after it has been created. */
						if (result && result.location) {
							data.url = result.location;
							data.coin = result.location.split("/")[2];
							editor.voidData.url = data.url;
							editor.voidData.coin = data.coin
							for (var i=0; i<data.characters.list.length; i++) {
								storeAction("add", {url :
								data.characters.list[i].character.url + "/conditions", data : {condition_id : data.coin}});
							}
							storeAction("redirect", {url : "conditions/#" + data.coin});
						}
					}});
				break;
				case "power":
					if(!editor.voidData.url) {
						addUrl = "powers";
						delete data.poin;
					}
					storeAction("add", {url : addUrl, data : data, callback : function(data, result) {
						/* Add the power to the specified owner after it has been created. */
						if (result && result.location) {
							data.url = result.location;
							data.poin = result.location.split("/")[2];
							editor.voidData.url = data.url;
							editor.voidData.poin = data.poin
							for (var i=0; i<data.characters.list.length; i++) {
								storeAction("add", {url : data.characters.list[i].character.url + "/powers", data : {power_id : data.poin}});
							}
							storeAction("redirect", {url : "powers/#" + data.poin});
						}
					}});
				break;
				case "item":
					if(!editor.voidData.url) {
						addUrl = "items";
						delete data.itin;
					}
					if (data.owner) {
						if(data.owner == "NONE") {
							data.plin = null;
							data.chin = null;
						} else {
							data.owner = data.owner.replace(/^[^\d]*/, '');
							data.owner = data.owner.replace(/[^\d]*$/, '');
							data.plin = data.owner.split(/[^\d]+/)[0];
							data.chin = data.owner.split(/[^\d]+/)[1];
							data.owner = data.plin + "/" + data.chin;
						}
					}
					storeAction("add", {url : addUrl, data : data, callback : function(data, result) {
						/* Add the attributes to the item after it has been created. */
						if (result && result.location) {
							data.url = result.location;
							data.itin = result.location.split("/")[2];
							editor.voidData.url = data.url;
							editor.voidData.itin = data.itin
							for (var i=0; i<data.attributes.list.length; i++) {
								storeAction("add", {url : data.url + "/attributes", data : {attribute_id : data.attributes.list[i].attribute.id}});
							}
							storeAction("redirect", {url : "items/#" + data.itin});
						}
					}});
				break;
				default:
					console.log("no save handler");
			}
			executeActions();
		break;
		case "voidwalker-unhide":
			csUnhide();
		break;
		case "voidwalker-history":
			if (!editor.voidData.url) {
				console.log("Hmm, geen pageData.url.  Nieuw / nog niet opgeslagen object?");
				return;
			}
			var parts = editor.voidData.url.split('/');
			parts[1] = parts[1].slice(0, -1);
			var historyUrl = voidBase + "admin/history" + parts.join('/');
			window.open(historyUrl, '_blank').focus();
		break;
		default:
			console.log("no handler for " + action);
		break;
	}
});

var chosenJsOptions = {
	max_shown_results : 15,
	disable_search_threshold : 5,
	search_contains : true
};

$(document).ready(function() {
	// Set the path to the hash on load to load the requested data;
	var path = document.location.pathname + document.location.hash.replace("#", "");
	document.body.setAttribute("data-simply-path", path);

	loadVoidData();

	$("select").each(function() {
		if (this.simplyValue) {
			this.value = this.simplyValue;
		}
	});
	$(".chosen-select").chosen(chosenJsOptions)
	.change(function(evt) {
		editor.fireEvent("databinding:valuechanged", this);
	});
	editor.voidData = editor.currentData[path];
});

window.addEventListener("hashchange", function(evt) {
	editor.currentData['/']['search-characters-players'] = [];
	editor.currentData['/']['search-items'] = [];
	editor.currentData['/']['search-powers-conditions'] = [];
	loadVoidData();

	$("select").each(function() {
		if (this.simplyValue) {
			this.value = this.simplyValue;
		}
	});

	$(".chosen-select").chosen(chosenJsOptions)
	.change(function(evt) {
		editor.fireEvent("databinding:valuechanged", this);
	});
});

search = function(keyword) {
	keyword = keyword.toLowerCase();

	var result = {
		"search-characters-players" : [],
		"search-items" : [],
		"search-powers-conditions" : []
	};

	if (keyword.search("^#[0-9]+$") > -1) {
		var target = editor.dataSources.characters.load.search("search > * > *", "plin[value='" + keyword.substr(1) + "'] ~ status[value='active']");
		if (target.length > 0) {
			document.location = walkerBase + "characters/#" + target[0].plin + "/" + target[0].chin;
			return;
		}
		keyword = keyword.substr(1);
	}
	if (keyword.search("^i[0-9]+$") > -1) {
		var target = editor.dataSources.items.load.search("search > * > *", "itin[value='" + keyword.substr(1) + "']");
		if (target.length > 0) {
			document.location = walkerBase + "items/#" + target[0].itin;
			return;
		}
		keyword = keyword.substr(1);
	}
	if (keyword.search("^p[0-9]+$") > -1) {
		var target = editor.dataSources.powers.load.search("search > * > *", "poin[value='" + keyword.substr(1) + "']");
		if (target.length > 0) {
			document.location = walkerBase + "powers/#" + target[0].poin;
			return;
		}
		keyword = keyword.substr(1);
	}
	if (keyword.search("^c[0-9]+$") > -1) {
		var target = editor.dataSources.conditions.load.search("search > * > *", "coin[value='" + keyword.substr(1) + "']");
		if (target.length > 0) {
			document.location = walkerBase + "conditions/#" + target[0].coin;
			return;
		}
		keyword = keyword.substr(1);
	}

	var i;

	var characters = editor.dataSources.characters.load.search('search > * > *', '[value*="' + keyword + '"]');
	for (i=0; i<characters.length; i++) {
		var character = characters[i];
		result["search-characters-players"].push({
			link : {
				href : walkerBase + "characters/#" + character.plin + "/" + character.chin,
				innerHTML : "characters/#" + character.plin + "/" + character.chin
			},
			title: characters[i].name + '<span class="character-status-icon">'+characterStatusIcon(characters[i].status)+'</span>',
			snippit: characters[i].playerName,
			id : characters[i].plin + "-" + characters[i].chin
		});
	}

	var players = editor.dataSources.players.load.search('search > * > *', '[value*="' + keyword + '"]');
	for (i=0; i<players.length; i++) {
		var player = players[i];
		result["search-characters-players"].push({
			link : {
				href : walkerBase + "players/#" + player.plin,
				innerHTML : "players/#" + player.plin
			},
			title : player.full_name,
			plin : player.plin
		});
	}

	var items = editor.dataSources.items.load.search('search > * > *', '[value*="' + keyword + '"]');
	for (i=0; i<items.length; i++) {
		var item = items[i];
		result["search-items"].push({
			link : {
				href : walkerBase + "items/#" + item.itin,
				innerHTML : "items/#" + item.itin
			},
			title : item.name,
			snippit: item['cs-text'],
			id : item.itin
		});
	}

	var powers = editor.dataSources.powers.load.search('search > * > *', '[value*="' + keyword + '"]');
	for (i=0; i<powers.length; i++) {
		var power = powers[i];
		result["search-powers-conditions"].push({
			link : {
				href : walkerBase + "powers/#" + power.poin,
				innerHTML : "powers/#" + power.poin
			},
			title : power.name,
			poin : power.poin
		});
	}
	var conditions = editor.dataSources.conditions.load.search('search > * > *', '[value*="' + keyword + '"]');
	for (i=0; i<conditions.length; i++) {
		var condition = conditions[i];
		result["search-powers-conditions"].push({
			link : {
				href : walkerBase + "conditions/#" + condition.coin,
				innerHTML : "conditions/#" + condition.coin
			},
			title : condition.name,
			coin : condition.coin
		});
	}

	return result;
};


searchListener = function(evt) {
	evt.preventDefault();
	editor.currentData['/']['search-characters-players'] = [];
	editor.currentData['/']['search-items'] = [];
	editor.currentData['/']['search-powers-conditions'] = [];

	var searchText = this.querySelector("input").value;
	if (searchText.length > 0) {
		var result = search(searchText);

		editor.currentData['/']['search-characters-players'] = result['search-characters-players'];
		editor.currentData['/']['search-items'] = result['search-items'];
		editor.currentData['/']['search-powers-conditions'] = result['search-powers-conditions'];

		if (result['search-characters-players'].length + result['search-items'].length + result['search-powers-conditions'].length === 0) {
			alert("Helaas, niks gevonden.");
		}
	}
};
document.getElementById("searchForm").addEventListener("submit", searchListener);

document.addEventListener("databind:elementresolved", function(evt) {
	/*
		This event is triggered by the databinding when the data in an
		element was set by the databinding.
	*/

	/* Notify chosen-selects that an update happened; */
	if (evt.target && evt.target.classList && evt.target.classList.contains(".chosen-select")) {
		$(evt.target).trigger("chosen:updated");
	}
	updateSkillPoints();
});
function updateSkillPoints() {
	if (document.getElementById("character-skillpoints-used")) {
		// Recalculate skill costs;
		var costs = document.querySelectorAll('input[data-simply-field="cost"]');
		var total = 0;
		for (var i=0; i<costs.length; i++) {
			total += parseFloat(costs[i].value);
		}
		document.getElementById("character-skillpoints-used").value = total;

		updateSkillListButtons();
	}
}
function updateSkillListButtons() {
	for(var i=0; i<editor.voidData.skills.list.length; i++) {
		var relation = editor.voidData.skills.list[i];
		var field = document.querySelector('button[data-action="voidwalker-plus-skill"] > input[value="'+relation.skill.id+'"]')
		if(!field) {
			continue;
		}
		var button = field.parentNode;

		if(relation.skill.times_max == 1) {
			if(!button.classList.contains('hidden')) {
				button.classList.add('hidden');
			}
		} else {
			if(button.classList.contains('hidden')) {
				button.classList.remove('hidden');
			}
		}

		button = document.querySelector('button[data-action="voidwalker-remove-skill"] > input[value="'+relation.skill.id+'"]').parentNode;
		button = button.querySelector('.glyphicon');
		if(relation.times == 1) {
			button.classList.remove('glyphicon-minus');
			if(!button.classList.contains('glyphicon-remove')) {
				button.classList.add('glyphicon-remove');
			}
		} else {
			button.classList.remove('glyphicon-remove');
			if(!button.classList.contains('glyphicon-minus')) {
				button.classList.add('glyphicon-minus');
			}
		}
	}
}

/*
	Helpers for API calls (login, get, put, delete); Just simple
	wrappers for HTTP calls. All take callbacks so you can do something
	with the result of the request when it is done.
*/
function voidLogin(callback) {
	if (validateToken()) {
		document.body.classList.add("logged-in");
		return callback();
	}

	if (!voidUser && !voidPass) {
		if (document.location.pathname !== walkerPath) {
			document.location.href = walkerBase;
		}
		return;
	}

	var data = {id: voidUser, password: voidPass};
	voidPut("auth/login", data, function(result) {
		if (result.token) {
			var payload = JSON.parse(atob(result.token.replace(/^.*\.(.*)\..*$/, '$1')));

			localStorage.token = result.token;
			localStorage.expiry = payload.exp;
			localStorage.name = result.name = payload.name;
			localStorage.plin = result.plin = payload.sub;
			localStorage.role = result.role = payload.role;
			callback(result);
		}
	});
}
function voidLoginStatus(result, warning = false) {
	var statusdiv = document.querySelector("#login-status");
	if(!statusdiv) {
		return;
	}

	statusdiv.style = '';
	var msgdiv = statusdiv.children[0]
	msgdiv.textContent = result.message;
	msgdiv.classList.remove('alert-warning');
	msgdiv.classList.remove('alert-danger');
	if(warning) {
		msgdiv.classList.add('alert-warning');
	} else {
		msgdiv.classList.add('alert-danger');
	}
}

function voidLogout() {
	delete localStorage.token;
	delete localStorage.expiry;
	delete localStorage.name;
	delete localStorage.plin;
	delete localStorage.role;
}

function voidGet(url, callback) {
	var http = new XMLHttpRequest();

	http.open("GET", voidBase + url, true);

	if (localStorage.token) {
		http.setRequestHeader("Authorization", "Bearer " +  localStorage.token);
	}

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4) {
			if (http.status > 199 && http.status < 300) {
				callback(JSON.parse(http.responseText));
			} else if (http.status == 401) {
				voidLoginStatus(JSON.parse(http.responseText));
			}
		}
	};
	http.send();
}

function voidPut(url, data, callback) {
	if (typeof callback !== "function") {
		callback = function() {};
	}
	var http = new XMLHttpRequest();

	http.open("PUT", voidBase + url, true);
	http.setRequestHeader("Content-type", "application/json");

	if (localStorage.token) {
		http.setRequestHeader("Authorization", "Bearer " +  localStorage.token);
	}

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4) {
			if (http.status === 0) {
				alert("Geen netwerkverbinding, probeer het straks nogmaals");
				return; // Network error, keep the changes for later;
			}
			if (http.status > 199 && http.status < 300) {
				if (http.status == 201) { // created
					callback({location : http.getResponseHeader("Location") });
				} else {
					callback(JSON.parse(http.responseText));
				}
			} else if (http.status == 401) {
				voidLoginStatus(JSON.parse(http.responseText));
			}
		}
	};
	http.send(JSON.stringify(data));
}

function voidPost(url, data, callback) {
	if (typeof callback !== "function") {
		callback = function() {};
	}
	var http = new XMLHttpRequest();

	http.open("POST", voidBase + url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	if (localStorage.token) {
		http.setRequestHeader("Authorization", "Bearer " +  localStorage.token);
	}

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4) {
			if (http.status === 0) {
				alert("Geen netwerkverbinding, probeer het straks nogmaals");
				return; // Network error, keep the changes for later;
			}
			if (http.status > 199 && http.status < 300) {
				callback(JSON.parse(http.responseText));
			}
		}
	};
	http.send(data);
}

function voidDelete(url, callback) {
	if (typeof callback !== "function") {
		callback = function() {};
	}
	var http = new XMLHttpRequest();

	http.open("DELETE", voidBase + url, true);
	http.setRequestHeader("Authorization", "Bearer " +  localStorage.token);
	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4) {
			if (http.status === 0) {
				alert("Geen netwerkverbinding, probeer het straks nogmaals");
				return; // Network error, keep the changes for later;
			}

		//	if (http.status > 199 && http.status < 300) {
				callback();
		//	}
		}
	};
	http.send();
}

function loadVoidData() {

	resetActions();

	var mode = pageMode();
	if (mode != "root" && document.location.hash.length < 2) {
		csUnhide();
		return;
	}

	document.body.classList.add("loading");
	var currentPath = document.body.getAttribute("data-simply-path");
	switch (mode) {
		case "root":
			document.body.classList.remove("loading");

			socialHandleCallback();
			delete localStorage.social_state;
			delete localStorage.social_login;

			voidLogin(function() {
				var data = [];
				data.token = localStorage.token;
				if(data.token) {
					data.name = '';
					data.link = {href: walkerBase + "players/#" + localStorage.plin, innerHTML: localStorage.name};
				}

				editor.currentData[currentPath] = data;
				editor.voidData = editor.currentData[currentPath];
				editor.data.apply(editor.currentData, document);
			});
		break;
		case "player":
			voidLogin(function() {
				voidGet("players/" + document.location.hash.replace(/^#/, ''), function(data) {
					var i;
					for (i=0; i<data.characters.list.length; i++) {
						character = data.characters.list[i];
						character.link = {href: walkerBase + "characters/#" + character.plin + "/" + character.chin};
						character.statusIcon = characterStatusIcon(character.status);
					}
					if (data.email === null) {
						data.email = "";
					}

					editor.currentData[currentPath] = data;
					editor.voidData = editor.currentData[currentPath];
					editor.data.apply(editor.currentData, document);

					$('input[name="new"]').val('');
					document.body.classList.remove("loading");
				});
			});
		break;
		case "character":
			voidLogin(function() {
				voidGet("characters/" + document.location.hash.replace(/^#/, ''), function(data) {
					var i;
					for (i=0; i<data.skills.list.length; i++) {
						data.skills.list[i].name = data.skills.list[i].skill.name;
						if(data.skills.list[i].skill.times_max > 1)
							data.skills.list[i].name += ' (' + data.skills.list[i].times + 'x)';
						data.skills.list[i].cost = data.skills.list[i].times * data.skills.list[i].skill.cost;
						if(data.skills.list[i].skill.mana_amount > 0)
							data.skills.list[i].mana_amount = data.skills.list[i].times * data.skills.list[i].skill.mana_amount;
					}
					for (i=0; i<data.items.list.length; i++) {
						data.items.list[i].link = {href: walkerBase + "items/#" + data.items.list[i].itin};
					}
					for (i=0; i<data.powers.list.length; i++) {
						data.powers.list[i].link = {href: walkerBase + "powers/#" + data.powers.list[i].power.poin};
						data.powers.list[i].power.link = {href: walkerBase + "powers/#" + data.powers.list[i].power.poin};
					}
					for (i=0; i<data.conditions.list.length; i++) {
						data.conditions.list[i].link = {href: walkerBase + "conditions/#" + data.conditions.list[i].condition.coin};
						data.conditions.list[i].condition.link = {href: walkerBase + "conditions/#" + data.conditions.list[i].condition.coin};
					}
					if (data.referee_notes === null) {
						data.referee_notes = "";
					}
					if (data.notes === null) {
						data.notes = "";
					}

					editor.currentData[currentPath] = data;
					editor.voidData = editor.currentData[currentPath];

					checkRole();
					editor.data.apply(editor.currentData, document);
					updateSkillPoints();
					$('input[name="new"]').val('');

					document.body.classList.remove("loading");
				});
			});
		break;
		case "condition":
			voidLogin(function() {
				voidGet("conditions/" + document.location.hash.replace(/^#/, ''), function(data) {
					var i, j;
					j = (data.characters ? data.characters.list.length : 0);
					for (i=0; i<j; i++) {
						data.characters.list[i].character.link = {href: walkerBase + "characters/#" + data.characters.list[i].character.plin + "/" + data.characters.list[i].character.chin};
						data.characters.list[i].character.statusIcon = characterStatusIcon(data.characters.list[i].character.status);
					}
					if (data.player_text === null) {
						data.player_text = "";
					}
					if (data.referee_notes === null) {
						data.referee_notes = "";
					}
					if (data.notes === null) {
						data.notes = "";
					}

					editor.currentData[currentPath] = data;
					editor.voidData = editor.currentData[currentPath];

					checkRole();
					editor.data.apply(editor.currentData, document);
					$('input[name="new"]').val('');

					document.body.classList.remove("loading");
				});
			});
		break;
		case "power":
			voidLogin(function() {
				voidGet("powers/" + document.location.hash.replace(/^#/, ''), function(data) {
					var i, j;
					j = (data.characters ? data.characters.list.length : 0);
					for (i=0; i<j; i++) {
						data.characters.list[i].character.link = {href: walkerBase + "characters/#" + data.characters.list[i].character.plin + "/" + data.characters.list[i].character.chin};
						data.characters.list[i].character.statusIcon = characterStatusIcon(data.characters.list[i].character.status);
					}
					if (data.player_text === null) {
						data.player_text = "";
					}
					if (data.referee_notes === null) {
						data.referee_notes = "";
					}
					if (data.notes === null) {
						data.notes = "";
					}

					editor.currentData[currentPath] = data;
					editor.voidData = editor.currentData[currentPath];

					checkRole();
					editor.data.apply(editor.currentData, document);
					$('input[name="new"]').val('');

					document.body.classList.remove("loading");
				});
			});
		break;
		case "item":
			voidLogin(function() {
				voidGet("items/" + document.location.hash.replace(/^#/, ''), function(data) {
					if (data.player_text === null) {
						data.player_text = "";
					}
					if (data.referee_notes === null) {
						data.referee_notes = "";
					}
					if (data.notes === null) {
						data.notes = "";
					}
					if (data.expiry === null) {
						data.expiry = "";
					}
					if (data.plin !== null && data.chin !== null) {
						data.owner = data.plin + "/" + data.chin;
						data.ownerLink = {href: walkerBase + "characters/#" + data.owner, innerHTML: data.owner};
					} else {
						data.owner = "";
						data.ownerLink = {href: '', innerHTML: ''};
					}

					editor.currentData[currentPath] = data;
					editor.voidData = editor.currentData[currentPath];

					checkRole();
					editor.data.apply(editor.currentData, document);
					$('input[name="new"]').val('');

					document.body.classList.remove("loading");
				});
			});
		break;
	}
}

function characterStatusIcon(characterStatus)
{
	switch(characterStatus) {
		case 'active':		return '\u2713';
		case 'inactive':	return '';
		case 'dead':		return '\u271D';
		default:			return '';
	};
}

function addMonths(dateStr, num) {
	var date = new Date(dateStr);
	date.setUTCMonth(num + date.getUTCMonth(), date.getUTCDate());
	return date.toJSON().substring(0, 10);
}

/*
	Log in and fetch all the static lists from the API (worlds, groups,
	believes, items etc; We only need to fetch these once, and they will
	be reused in the page as long as we don't reload.
*/
voidLogin(function() {
	voidGet("worlds", function(data) {
		jsonCSS.init(data.list);
		editor.addDataSource("worlds", {
			load : data.list,
			applyOnce : true
		});
	});

	voidGet("groups", function(data) {
		jsonCSS.init(data.list);
		editor.addDataSource("groups", {
			load : data.list,
			applyOnce : true
		});
	});

	voidGet("factions", function(data) {
		var options = [];
		for (var i=0; i<data.list.length; i++) {
			options.push({
				option : {
					innerHTML : data.list[i].name,
					value : data.list[i].id
				}
			});
		}

		editor.addDataSource("factionOptions", {
			load : options,
			set : function(list, data) {
				list.value = list.simplyValue;
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});

		jsonCSS.init(data.list);
		editor.addDataSource("factions", {
			load : data.list,
			set : function(list, data) {
				list.value = list.simplyValue;
			},
			applyOnce : true
		});
	});

	voidGet("believes", function(data) {
		jsonCSS.init(data.list);
		editor.addDataSource("believes", {
			load : data.list,
			applyOnce : true
		});
	});

	voidGet("skills", function(data) {
		jsonCSS.init(data.list);
		editor.addDataSource("skills", {
			load : data.list,
			set : function(list, data) {
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
	});

	voidGet("attributes", function(data) {
		var options = [];
		for (var i=0; i<data.list.length; i++) {
			if (data.list[i].name) {
				options.push({
					option : {
						innerHTML : data.list[i].name + " (" + data.list[i].code + ")",
						value : data.list[i].code
					}
				});
			}
		}

		jsonCSS.init(data.list);
		editor.addDataSource("attributes", {
			load : data.list,
			set : function(list, data) {
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
		editor.addDataSource("attributeOptions", {
			load : options,
			set : function(list, data) {
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
	});

	voidGet("players", function(data) {
		jsonCSS.init(data.list);
		editor.addDataSource("players", {
			load : data.list,
			applyOnce : true
		});
	});

	voidGet("characters/", function(data) {
		var options = [];
		for (var i=0; i<data.list.length; i++) {
			options.push({
				option : {
					innerHTML : data.list[i].plin + "-" + data.list[i].chin + " - " + data.list[i].name,
					value : data.list[i].plin + "/" + data.list[i].chin
				}
			});
			data.list[i].statusIcon = characterStatusIcon(data.list[i].status);
		}

		editor.addDataSource("characterOptions", {
			load : options,
			set : function(list, data) {
				list.value = list.simplyValue;
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
		jsonCSS.init(data.list);
		editor.addDataSource("characters", {
			load : data.list,
			set : function(list, data) {
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
	});

	voidGet("items", function(data) {
		var options = [];
		var optionsHtml = '';
		for (var i=0; i<data.list.length; i++) {
			optionsHtml += '<option value="' + data.list[i].itin + '">' + data.list[i].itin + " - " + data.list[i].name + '</option>';
		}

		jsonCSS.init(data.list);
		editor.addDataSource("items", {
			load : data.list,
			set : function(list, data) {
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
		editor.addDataSource("itemOptions", {
			load : function(list, callback) {
				list.innerHTML += optionsHtml;
			},
			set : function(list, data) {
				editor.fireEvent("chosen:updated", list);
			},
			applyOnce : true
		});
	});

	var powerOptions = '';
	var conditionOptions = '';
	voidGet("powers", function(data) {
		var optionsHtml = '';
		for (var i=0; i<data.list.length; i++) {
			data.list[i].pcin = data.list[i].poin;
			optionsHtml += '<option value="p' + data.list[i].poin + '">' + data.list[i].poin + " - " + data.list[i].name + '</option>';
		}
		powerOptions = optionsHtml;

		jsonCSS.init(data.list);
		editor.addDataSource("powers", {
			load: data.list,
			applyOnce : true
		});
		if (powerOptions.length > 0 && conditionOptions.length > 0) {
			editor.addDataSource("powersConditionsOptions", {
				load : function(list, callback) {
					list.innerHTML += powerOptions + conditionOptions;
				},
				set : function(list, data) {
					editor.fireEvent("chosen:updated", list);
				},
				applyOnce : true
			});
		}
	});
	voidGet("conditions", function(data) {
		var optionsHtml = '';

		for (var i=0; i<data.list.length; i++) {
			data.list[i].pcin = data.list[i].coin;
			optionsHtml += '<option value="c' + data.list[i].coin + '">' + data.list[i].coin + " - " + data.list[i].name + '</option>';
		}

		conditionOptions = optionsHtml;
		jsonCSS.init(data.list);
		editor.addDataSource("conditions", {
			load: data.list,
			applyOnce : true
		});
		if (powerOptions.length > 0 && conditionOptions.length > 0) {
			editor.addDataSource("powersConditionsOptions", {
				load : function(list, callback) {
					list.innerHTML += powerOptions + conditionOptions;
				},
				set : function(list, data) {
					editor.fireEvent("chosen:updated", list);
				},
				applyOnce : true
			});
		}
	});
});

/* Keyboard shortcut handlers */
document.addEventListener("keydown", function(evt) {
	var key = evt.keyCode || evt.which;
	if (key == 27) { // ESC
		evt.preventDefault();
		// Clear search results;
		editor.currentData['/']['search-characters-players'] = [];
		editor.currentData['/']['search-items'] = [];
		editor.currentData['/']['search-powers-conditions'] = [];
	} else if (key == 70 && evt.ctrlKey && !evt.shiftKey && !evt.altKey) { // Ctrl-f
		evt.preventDefault();
		var searchInput = document.querySelector("#searchForm input");
		if (searchInput) {
			searchInput.focus();
		}
	} else if (key == 80 && evt.ctrlKey && !evt.shiftKey && !evt.altKey) { // Ctrl-p
		evt.preventDefault();
		var printButton = document.querySelector("[data-action=voidwalker-print],[data-action=voidwalker-print-all]");
		editor.fireEvent("click", printButton);
	} else if (key == 83 && evt.ctrlKey && !evt.shiftKey && !evt.altKey) { // Ctrl-s
		evt.preventDefault();
		var saveButton = document.querySelector("[data-action=voidwalker-save]");
		editor.fireEvent("click", saveButton);
	}
});

function checkRole()
{
	if (localStorage.role == "Referee" || localStorage.role == "Read-only") {
		csUnhide();
	} else {
		csHide();
	}
}

function csHide() {
	var elems = document.querySelectorAll(".cs-only");
	for (var i=0; i<elems.length; i++) {
		elems[i].classList.add("cs-only-hidden");
		elems[i].classList.remove("cs-only");
	}
}

function csUnhide() {
	var elems = document.querySelectorAll(".cs-only-hidden");
	for (var i=0; i<elems.length; i++) {
		elems[i].classList.add("cs-only");
		elems[i].classList.remove("cs-only-hidden");
	}
}

if ('serviceWorker' in navigator) {
	navigator.serviceWorker.getRegistrations()
	.then(function(registrations) {
		for(let registration of registrations) {
			if(registration.scope == walkerBase)
				registration.unregister();
		}
	});
}

/**
 *  Howto login via a third party / social login provider.
 *
 *  Call voidGET('/auth/social').
 *  It returns a list of all the supported social login providers.
 *  For each provider there's a 'url' and 'authUri' link, both need to be
 *  customized by the front-end before they can be used.
 *
 *  The 'authUri' is used first.
 *  Replace the STATE and CALLBACK strings in 'authUri':
 *    - STATE is a random string used to prefent cross-site request forgery
 *    - CALLBACK is where the user gets redirect to after login
 *  Redirect the user to 'authUri' to start the login proces.
 *
 *  If succesful, the user get's redirected back to our CALLBACK location.
 *  First we check that the 'state' matches with our expected value.
 *  Next replace CODE and CALLBACK in the 'url' of the social provider:
 *    - CODE is the string we got in the query parameter after the login
 *    - CALLBACK must be the same as used in the 'authUri'
 *  Perform a GET on the 'url' should yield a JWT that can be used for all
 *  following interactions with the void api.
 *  I.e. the user is logged in now.
 *
 *  The url of the front-end that is used in CALLBACK needs to be configured
 *  as a know 'redirect_uri' at the third party / social login provider.
 *
 *  TODO's:
 *    - call voidGet('/auth/social'), instead of hardcoding urls...
 *    - handle error's from the social login page return redirect
 *    - handle error's from the void-api login-with-code GET
 */
function socialAuthentication(authUri, login) {
	const callbackURI = encodeURI(walkerBase);

	localStorage.social_state = generateState(16)
	localStorage.social_login = login.replace('CALLBACK', encodeURI(callbackURI));

	let uri = decodeURI(authUri);
	uri = uri.replace('CALLBACK', callbackURI)
	uri = uri.replace('STATE', localStorage.social_state);

	document.location = uri;
}

function socialHandleCallback() {
	const urlParams = new URLSearchParams(window.location.search);
	const code = urlParams.get('code');
	const state = urlParams.get('state');

	if(!state || !code) {
		return;
	}

	if(state != localStorage.social_state) {
		// check against cross-site request forgery
		console.log("MISMATCH!");
		console.log("got      "+state);
		console.log("expected "+localStorage.social_state);
		return;
	}

	voidLoginStatus({'message': 'Processing social login ...'}, true);

	let loginUrl = localStorage.social_login.replace('CODE', code);
	voidGet(loginUrl, function(result) {
		if (result.token) {
			var payload = JSON.parse(atob(result.token.replace(/^.*\.(.*)\..*$/, '$1')));

			localStorage.token = result.token;
			localStorage.expiry = payload.exp;
			localStorage.name = result.name = payload.name;
			localStorage.plin = result.plin = payload.sub;
			localStorage.role = result.role = payload.role;
			document.location = walkerBase + "players/#" + result.plin;
		}
	});
}

function generateState(len) {
	let result = '';
	for (let i = 0; i < len; ++i) {
		result += (Math.floor(Math.random() * 16)).toString(16);
	}
	return result;
}
