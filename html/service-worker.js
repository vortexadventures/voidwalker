// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var cacheName = 'voidwalker-v1';
var dataUrl = 'https://api.the-vortex.nl/';
var shellUrl = 'https://voidwalker.the-vortex.nl';

var filesToCache = [
	'/',
	'/index.html',
	'/players/',
	'/players/index.html',
	'/characters/',
	'/characters/index.html',
	'/conditions/',
	'/conditions/index.html',
	'/powers/',
	'/powers/index.html',
	'/items/',
	'/items/index.html',

	'/voidwalker.css',
	'/voidwalker.js',
	"/libs/bootstrap-3.3.7-dist/css/bootstrap.min.css",
	"/libs/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css",
	"/libs/bootstrap-3.3.7-dist/fonts/glyphicons-halflings-regular.eot",
	"/libs/bootstrap-3.3.7-dist/fonts/glyphicons-halflings-regular.svg",
	"/libs/bootstrap-3.3.7-dist/fonts/glyphicons-halflings-regular.ttf",
	"/libs/bootstrap-3.3.7-dist/fonts/glyphicons-halflings-regular.woff",
	"/libs/bootstrap-3.3.7-dist/fonts/glyphicons-halflings-regular.woff2",
	"/libs/bootstrap-3.3.7-dist/js/bootstrap.min.js",
	'/libs/chosen-v1.6.2/chosen-sprite.png',
	'/libs/chosen-v1.6.2/chosen.min.css',
	'/libs/chosen-v1.6.2/chosen.jquery.min.js',
	"/libs/jquery-1.12.4-dist/jquery.min.js",
	'/libs/json-css.js',
	'/libs/simplyedit-1.21/simply-edit.js',
	'/libs/simplyedit-1.21/simply/databind.js?v=@version'
];

var externalFilesToCache = [
//	"https://cdn.simplyedit.io/1/simply-edit.js",
//	"https://cdn.simplyedit.io/1/simply/databind.js?v=@version"
];

self.addEventListener('install', function(e) {
	console.log('[ServiceWorker] Install');
	e.waitUntil(caches.open(cacheName).then(function(cache) {
		console.log('[ServiceWorker] Caching app shell');
		cache.addAll(externalFilesToCache);
		return cache.addAll(filesToCache.map(url => new Request(shellUrl + url, {credentials: 'same-origin'})));
	}));
});

self.addEventListener('activate', function(e) {
	console.log('[ServiceWorker] Activate');
	e.waitUntil(caches.keys().then(function(keyList) {
		return Promise.all(keyList.map(function(key) {
			if (key !== cacheName && key !== cacheName) {
				console.log('[ServiceWorker] Removing old cache', key);
				return caches.delete(key);
			}
		}));
	}));
	/*
	 * Fixes a corner case in which the app wasn't returning the latest data.
	 * You can reproduce the corner case by commenting out the line below and
	 * then doing the following steps: 1) load app for first time so that the
	 * initial New York City data is shown 2) press the refresh button on the
	 * app 3) go offline 4) reload the app. You expect to see the newer NYC
	 * data, but you actually see the initial data. This happens because the
	 * service worker is not yet activated. The code below essentially lets
	 * you activate the service worker faster.
	 */
	return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
	console.log('[ServiceWorker] Fetch', e.request.url);
	if (e.request.url.indexOf("auth/login") > -1) {
		e.respondWith(fetch(e.request).catch(function(e) {
			console.log("Fallback login");
			var fallbackResponse = {
				token : "offline"
			};
			return new Response(JSON.stringify(fallbackResponse), {
				headers: {'Content-Type': 'application/json'}
			});
		}));

	} else if (
		e.request.method == "GET" &&
		(
			e.request.url.indexOf(dataUrl) > -1 &&
			e.request.url.match(/(players|characters|spells|skills|attributes|items|powers|conditions|worlds|groups|factions|believes)\/?$/)
		) ||
		(
			e.request.url.indexOf(shellUrl) > -1
		)
	) {
		/*
		 * When the request URL contains dataUrl, the app is asking for fresh
		 * data. In this case, the service worker always goes to the network
		 * and then caches the response.
		 * This is called the "Cache then network" strategy:
		 * https://jakearchibald.com/2014/offline-cookbook/#cache-then-network
		 */
		e.respondWith(caches.match(e.request).then(function(response) {
			if (response) {
				console.log('[ServiceWorker] Using cached response for', e.request.url);
				fetch(e.request).then(function(response) {
					caches.open(cacheName).then(function(cache) {
						cache.put(e.request, response.clone());
					});
					console.log('[ServiceWorker] Updated cache for', e.request.url);
				}).catch(function(e) {
					console.log('[ServiceWorker] Update cache failed.');
					console.log(e);
				});
				return response;
			} else {
				console.log('[ServiceWorker] Falling back to network for', e.request.url);
				return fetch(e.request).then(function(response) {
					caches.open(cacheName).then(function(cache) {
						cache.put(e.request, response.clone());
					});
					return response.clone();
				}).catch(function(e) {
					console.log('[ServiceWorker] Fetch failed.');
					console.log(e);
				})
			}
		}).catch(function() {
			console.log('[ServiceWorker] Not in cache');
		}));

	} else if (e.request.url.indexOf("data.json") > -1) {
		e.respondWith(fetch(e.request).catch(function(e) {
			console.log('Fallback data.json');
			var fallbackResponse = {};
			return new Response(JSON.stringify(fallbackResponse), {
				headers: {'Content-Type': 'application/json'}
			});
		}));

	} else { // if (e.request.url.indexOf(shellUrl) > -1) {
		/*
		 * The app is asking for app shell files. In this scenario the app
		 * uses the "Cache, falling back to the network" offline strategy:
		 * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
		 */
		e.respondWith(caches.match(e.request).then(function(response) {
			return response || fetch(e.request);
		}));
	}
});
